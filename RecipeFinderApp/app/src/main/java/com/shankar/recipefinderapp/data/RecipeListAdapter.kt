package com.shankar.recipefinderapp.data

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.shankar.recipefinderapp.R
import com.shankar.recipefinderapp.activities.ShowLinkActivity
import com.shankar.recipefinderapp.model.Recipe
import com.squareup.picasso.Picasso

class RecipeListAdapter(private val list: ArrayList<Recipe>, private val context: Context): RecyclerView.Adapter<RecipeListAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.recipe_list_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(list[position])
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var titleText = itemView.findViewById<TextView>(R.id.recipeTitle)
        var ingredientsText = itemView.findViewById<TextView>(R.id.recipeIngredients)
        var thumbnail = itemView.findViewById<ImageView>(R.id.imageView)
        var linkButton = itemView.findViewById<Button>(R.id.linkButton)

        fun bindView(recipe: Recipe){
            titleText.text = recipe.recipeName
            ingredientsText.text = recipe.recipeIngredients
            linkButton.setOnClickListener {
                var intent = Intent(context, ShowLinkActivity::class.java)
                intent.putExtra("link", recipe.recipeLink.toString())
                context.startActivity(intent)
            }
            //To add image from image url to mage view using Picasso library
            if(!TextUtils.isEmpty(recipe.thumbnail)){
                Picasso.with(context)
                        .load(recipe.thumbnail)
                        .placeholder(android.R.drawable.ic_menu_report_image)
                        .error(android.R.drawable.ic_menu_report_image)
                        .into(thumbnail)
            }
            else{
                Picasso.with(context)
                        .load(R.mipmap.ic_launcher)
                        .into(thumbnail)
            }
        }
    }

}