package com.shankar.recipefinderapp.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.shankar.recipefinderapp.R
import com.shankar.recipefinderapp.data.LEFT_LINK
import com.shankar.recipefinderapp.data.QUERY
import com.shankar.recipefinderapp.data.RecipeListAdapter
import com.shankar.recipefinderapp.model.Recipe
import kotlinx.android.synthetic.main.activity_recipe_list.*
import org.json.JSONException
import org.json.JSONObject

class RecipeList : AppCompatActivity() {

    var vollyRequest: RequestQueue? = null
    var recipeList: ArrayList<Recipe>? = null
    val requestUrl = "http://www.recipepuppy.com/api/?i=onions,garlic&q=omelet&p=3"
    var recipeAdapter: RecipeListAdapter? = null
    var layoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_list)

        var url:String?
        var extras = intent.extras
        var ingredients = extras.get("ingredients")
        var searchTerm = extras.get("search")
        if(extras != null && !extras.get("ingredients").equals("") && !extras.get("search").equals("")){
            //construct url
            var tempURL = LEFT_LINK + ingredients + QUERY + searchTerm
            url = tempURL
        }
        else{
            url = requestUrl
        }


        recipeList = ArrayList<Recipe>()

        vollyRequest = Volley.newRequestQueue(this)
        getRecipe(url)
    }

    fun getRecipe(url: String) {
        val recipeRequest = JsonObjectRequest(Request.Method.GET, url, Response.Listener { response: JSONObject ->
            try {
                val resultArray = response.getJSONArray("results")
                for (i in 0..resultArray.length() - 1){
                    val recipeObj = resultArray.getJSONObject(i)
                    var title = recipeObj.getString("title")
                    var link = recipeObj.getString("href")
                    var thumbnail = recipeObj.getString("thumbnail")
                    var ingredients = recipeObj.getString("ingredients")
                    var recipe = Recipe(title,ingredients,thumbnail,link)
                    recipeList!!.add(recipe)

                    Log.d("Result", thumbnail)
                }
                recipeAdapter = RecipeListAdapter(recipeList!!, this)
                layoutManager = LinearLayoutManager(this)

                //Setup recycler view
                recyclerView.layoutManager = layoutManager
                recyclerView.adapter = recipeAdapter
                recipeAdapter!!.notifyDataSetChanged()

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { error: VolleyError? ->
            try {
                Log.d("Error:" , error.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
        vollyRequest!!.add(recipeRequest)
    }
}
