package com.shankar.recipefinderapp.model

class Recipe {
    var recipeName: String? = null
    var recipeIngredients: String? = null
    var thumbnail: String? = null
    var recipeLink: String? = null

    constructor(recipeName: String?, recipeIngredients: String?, thumbnail: String?, recipeLink: String?) {
        this.recipeName = recipeName
        this.recipeIngredients = recipeIngredients
        this.thumbnail = thumbnail
        this.recipeLink = recipeLink
    }
}