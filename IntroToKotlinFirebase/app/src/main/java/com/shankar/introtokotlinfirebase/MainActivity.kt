package com.shankar.introtokotlinfirebase

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private var currentUser: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var firebaseDatabase = FirebaseDatabase.getInstance()
        var databaseReference = firebaseDatabase.getReference("/users").push()
        mAuth = FirebaseAuth.getInstance()

        //Sign up new user
        createAccountID.setOnClickListener{
            var email = emailET.text.toString().trim()
            var password = passwordET.text.toString().trim()
            mAuth!!.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener(this,{
                        task: Task<AuthResult> ->
                        if(task.isSuccessful){
                            var user:FirebaseUser = mAuth!!.currentUser!!
                            Log.d("USer", user.email)
                        }
                        else
                        {
                            Log.d("Error ", task.exception.toString())
                        }
                    })
        }
        //Sign existing users in
        mAuth!!.signInWithEmailAndPassword("abc@xyz.com", "123456")
                .addOnCompleteListener { task: Task<AuthResult> ->
                    if (task.isSuccessful) {
                        //Sign in was successful
                        Toast.makeText(this, "Signed in successfully!", Toast.LENGTH_SHORT).show()
                    } else {
                        //Sign in was not successful
                        Toast.makeText(this, "Sign in failed!", Toast.LENGTH_SHORT).show()
                    }
                }


        //Write to DB
//        var employee = Employee("Sangeetha", "Android Developer", "Ernakulam", 19)
//        var employee = Employee("Prabhashankar", "Android Developer", "Ernakulam", 20)
//        databaseReference.setValue(employee)    //adding an object to db
//
//        databaseReference.setValue("Hello There")
//
//        //Read from DB
//        databaseReference.addValueEventListener(object: ValueEventListener{
//            override fun onDataChange(dataSnapshot: DataSnapshot?) {
//                var value = dataSnapshot!!.value as HashMap<String, Any>
//
//                Log.d("Value ", value.get("name").toString())
//            }
//            override fun onCancelled(error: DatabaseError?) {
//                Log.d("Error" , error!!.message)
//            }
//        })
//    }
//    data class Employee(var name:String, var position: String, var homeAddress:String, var age:Int)

    }

    override fun onStart() {
        super.onStart()
        currentUser = mAuth!!.currentUser
        //call a function to update the UI with current user
        if (currentUser != null) {
            Toast.makeText(this, "User is logged in!", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "User is logged out!", Toast.LENGTH_SHORT).show()
        }
    }
}

