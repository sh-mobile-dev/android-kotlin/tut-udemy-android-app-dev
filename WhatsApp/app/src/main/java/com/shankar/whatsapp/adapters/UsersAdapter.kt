package com.shankar.whatsapp.adapters

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.shankar.whatsapp.R
import com.shankar.whatsapp.activities.ChatActivity
import com.shankar.whatsapp.activities.ProfileActivity
import com.shankar.whatsapp.models.Users
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class UsersAdapter(firebaseRecyclerOptions: FirebaseRecyclerOptions<Users>, var context: Context)
    : FirebaseRecyclerAdapter<Users, UsersAdapter.ViewHolder>(firebaseRecyclerOptions) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.users_row, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int, user: Users) {
        var userID = getRef(position).key   //gets unique firebase id for current user
        viewHolder.bindView(user, context)
        viewHolder.itemView.setOnClickListener {
            //Create a popup dialog where users can choose either send a message or view the profile
            var options = arrayOf("Open Profile", "Send Message")
            var builder = AlertDialog.Builder(context)
            builder.setTitle("Select Options")
            builder.setItems(options, DialogInterface.OnClickListener{
                //i is the index of options array
                dialogInterface, i ->
                var userName = viewHolder.userNameText
                var userStat = viewHolder.userStatusText
                var profilePic = viewHolder.userProfilePicLink
                if (i == 0){
                    //open profile
                    var profileIntent = Intent(context, ProfileActivity::class.java)
                    profileIntent.putExtra("user_id", userID)
                    context.startActivity(profileIntent)
                }
                else{
                    //send message : Chat Activity
                    var chatIntent = Intent(context, ChatActivity::class.java)
                    chatIntent.putExtra("user_id", userID)
                    chatIntent.putExtra("name", userName)
                    chatIntent.putExtra("status", userStat)
                    chatIntent.putExtra("image", profilePic)
                    context.startActivity(chatIntent)
                }
            })
            builder.show()
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var userNameText: String? = null
        var userStatusText: String? = null
        var userProfilePicLink: String? = null
        fun bindView(user: Users, context: Context) {
            var userName = itemView.findViewById<TextView>(R.id.usersName)
            var userStatus = itemView.findViewById<TextView>(R.id.usersStatus)
            var userImage = itemView.findViewById<CircleImageView>(R.id.usersProfileImage)

            //set the strings so we can pass in the intent
            userNameText = user.display_name
            userStatusText = user.status
            userProfilePicLink = user.thumb_image

            userName.text = userNameText
            userStatus.text = userStatusText

            Picasso.with(context)
                    .load(userProfilePicLink)
                    .placeholder(R.drawable.profile_img)
                    .into(userImage)

        }
    }

}