package com.shankar.whatsapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.shankar.whatsapp.R
import kotlinx.android.synthetic.main.activity_create_account.*

class CreateAccountActivity : AppCompatActivity() {

    var mAuth: FirebaseAuth? = null
    var mDatabase: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)
        mAuth = FirebaseAuth.getInstance()
        signUpButton.setOnClickListener {
            var email = signUpEmailET.text.toString().trim()
            var password = signUpPasswwordET.text.toString().trim()
            var displayName = signUpAccountName.text.toString().trim()
            if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(email)){
                if (password.length >= 6){
                    createAccount(email, password, displayName)
                }
                else{
                    Toast.makeText(this, "Password should be atleast 6 characters.", Toast.LENGTH_SHORT).show()
                }
            }
            else{
                Toast.makeText(this, "None of the fields can be empty", Toast.LENGTH_SHORT).show()
            }
        }
    }

    //Creates account in firebase
    fun createAccount(email:String, password:String, displayName:String){
        mAuth!!.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener{
                    task: Task<AuthResult> ->
                    if (task.isSuccessful){
                        var currentUser = mAuth!!.currentUser
                        var userID = currentUser!!.uid

                        mDatabase = FirebaseDatabase.getInstance().reference
                                .child("Users").child(userID)

                        var userObject = HashMap<String, String>()
                        userObject.put("display_name", displayName)
                        userObject.put("status", "Hello")
                        userObject.put("image","default")
                        userObject.put("thumb_image", "default")

                        mDatabase!!.setValue(userObject).addOnCompleteListener{
                            task: Task<Void> ->
                            if(task.isSuccessful){
                                Log.d("Sign Up", "Successful")
                                var dashboardIntent = Intent(this, DashboardActivity::class.java)
                                dashboardIntent.putExtra("display_name", displayName)
                                startActivity(dashboardIntent)
                                finish()
                            }
                            else{
                                Toast.makeText(this, "User Creation Failed", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                    else{
                        Toast.makeText(this, "User Creation Failed", Toast.LENGTH_SHORT).show()
                    }
                }
    }
}
