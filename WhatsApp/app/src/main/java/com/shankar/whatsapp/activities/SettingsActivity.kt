package com.shankar.whatsapp.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.shankar.whatsapp.R
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_settings.*
import java.io.ByteArrayOutputStream
import java.io.File

class SettingsActivity : AppCompatActivity() {

    var mDatabase: DatabaseReference? = null
    var mCurrentUser: FirebaseUser? = null
    var mStorageRef: StorageReference? = null

    var GALLERY_ID: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        mStorageRef = FirebaseStorage.getInstance().reference
        mCurrentUser = FirebaseAuth.getInstance().currentUser
        var userID = mCurrentUser!!.uid
        mDatabase = FirebaseDatabase.getInstance().reference
                .child("Users")
                .child(userID)
        mDatabase!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                var displayName = dataSnapshot!!.child("display_name").value
                var status = dataSnapshot!!.child("status").value
                var image = dataSnapshot!!.child("image").value
                var thumbnail = dataSnapshot!!.child("thumb_image").value

                settingsDisplayName.text = displayName.toString()
                settingsStatusText.text = status.toString()

                //To set thumbnail as profile image in settings activity
                if(!image!!.equals("default")){
                    Picasso.with(applicationContext)
                            .load(thumbnail.toString())
                            .placeholder(R.drawable.profile_img)
                            .into(settingsProfileImage)
                }

            }

            override fun onCancelled(databaseError: DatabaseError?) {

            }

        })
        settingsChangeStatus.setOnClickListener {
            var intent = Intent(this, StatusActivity::class.java)
            intent.putExtra("status", settingsStatusText.text.toString().trim())
            startActivity(intent)
        }
        settingsChangeProfileImage.setOnClickListener {
            var galleryIntent = Intent()
            galleryIntent.type = "image/*"  //what we are trying to get
            galleryIntent.action = Intent.ACTION_GET_CONTENT //we are trying to get some content
            startActivityForResult(Intent.createChooser(galleryIntent, "SELECT_IMAGE"), GALLERY_ID)
            //createChooser chooses something, SELECT_IMAGE is a title. GALLERY_ID is used in onActivityResult
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        if (requestCode == GALLERY_ID && resultCode == Activity.RESULT_OK) {
            var image: Uri = resultData!!.data
            CropImage.activity(image)
                    .setAspectRatio(1, 1)
                    .start(this)
        }
        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(resultData)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                var userId = mCurrentUser!!.uid
                var thumbFile = File(resultUri.path)

                //To compress thumbfile to bitmap using a library
                var thumbBitmap = Compressor(this)
                        .setMaxWidth(200)
                        .setMaxHeight(200)
                        .setQuality(65)
                        .compressToBitmap(thumbFile)

                //To upload file to Firebase Storage
                var byteArray = ByteArrayOutputStream()
                thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArray)
                var thumbByteArray: ByteArray
                thumbByteArray = byteArray.toByteArray()

                var filePath = mStorageRef!!.child("chat_profile_images")
                        .child(userId + ".jpg")

                //Create another directory for smaller compressed images
                var thumbFilePath = mStorageRef!!.child("chat_profile_images")
                        .child("thumbs")
                        .child(userId + ".jpg")
                filePath.putFile(resultUri)
                        .addOnCompleteListener{
                            task: Task<UploadTask.TaskSnapshot> ->
                            if(task.isSuccessful){
                                //Lets get the pic url
                                var downloadUrl = task.result.downloadUrl.toString()
                                //Upload task
                                var uploadTask: UploadTask = thumbFilePath.putBytes(thumbByteArray)
                                uploadTask.addOnCompleteListener{
                                    task: Task<UploadTask.TaskSnapshot> ->
                                    var thumbUrl = task.result.downloadUrl.toString()
                                    if (task.isSuccessful){
                                        var updateObject = HashMap<String, Any>()
                                        updateObject.put("image", downloadUrl)
                                        updateObject.put("thumb_image", thumbUrl)

                                        //We save the profile image
                                        mDatabase!!.updateChildren(updateObject)
                                                .addOnCompleteListener {
                                                    task: Task<Void> ->
                                                    if(task.isSuccessful){
                                                        Toast.makeText(this, "Profile image saved", Toast.LENGTH_SHORT).show()
                                                    }
                                                    else{
                                                        Toast.makeText(this, "Profile image updation failed", Toast.LENGTH_SHORT).show()
                                                    }
                                                }
                                    }
                                    else{
                                        Toast.makeText(this, "Profile image changing failed", Toast.LENGTH_SHORT).show()
                                    }
                                }
                            }
                        }
            }
        }
//        super.onActivityResult(requestCode, resultCode, data)

    }
}
