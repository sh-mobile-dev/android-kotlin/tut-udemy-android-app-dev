package com.shankar.whatsapp.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.shankar.whatsapp.R
import com.shankar.whatsapp.models.FriendlyMessage
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.custom_bar_image.*
import kotlinx.android.synthetic.main.custom_bar_image.view.*

class ChatActivity : AppCompatActivity() {

    var userId: String? = null
    var mFirebaseDatabase: DatabaseReference? = null
    var mFirebaseUser: FirebaseUser? = null

    var layoutManager: LinearLayoutManager? = null
    var mFirebaseAdapter: FirebaseRecyclerAdapter<FriendlyMessage, MessageViewHolder>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        mFirebaseUser = FirebaseAuth.getInstance().currentUser
        userId = intent.extras.getString("user_id")
        layoutManager = LinearLayoutManager(this)
        layoutManager!!.stackFromEnd = true

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        //Setting custom app bar
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        var inflater = this.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var actionBarView = inflater.inflate(R.layout.custom_bar_image,null)
        actionBarView.customBarName.text = intent.extras.getString("name")
        Picasso.with(this)
                .load(intent.extras.getString("image"))
                .placeholder(R.drawable.profile_img)
                .into(actionBarView.customBarCircleImage)
        supportActionBar!!.customView = actionBarView

        mFirebaseDatabase = FirebaseDatabase.getInstance().reference
        var mFirebaseDatabaseMessages = mFirebaseDatabase!!.child("messages")
        var firebaseRecyclerOptions = FirebaseRecyclerOptions.Builder<FriendlyMessage>()
                .setQuery(mFirebaseDatabaseMessages, FriendlyMessage::class.java)
                .build()
        mFirebaseAdapter = object : FirebaseRecyclerAdapter<FriendlyMessage, MessageViewHolder>(firebaseRecyclerOptions) {

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
                val view = LayoutInflater.from(applicationContext).inflate(R.layout.item_message, parent, false)
                return MessageViewHolder(view)
            }

            override fun onBindViewHolder(holder: MessageViewHolder, position: Int, friendlyMessage: FriendlyMessage) {
                if (friendlyMessage.message != null) {
                    holder.bindView(friendlyMessage)
                    var currentUserId = mFirebaseUser!!.uid
                    var isMe: Boolean = friendlyMessage.id.equals(currentUserId)
                    if (isMe) {
                        //Me to the right side
                        holder.profileImageViewRight!!.visibility = View.VISIBLE
                        holder.profileImageViewLeft!!.visibility = View.GONE
                        holder.messageTextView!!.gravity = (Gravity.CENTER_VERTICAL or Gravity.RIGHT)
                        holder.messengerTextView!!.gravity = (Gravity.CENTER_VERTICAL or Gravity.RIGHT)

                        mFirebaseDatabase!!.child("Users")
                                .child(currentUserId)
                                .addValueEventListener(object : ValueEventListener {
                                    override fun onDataChange(dataSnapshot: DataSnapshot?) {
                                        var imageUrl = dataSnapshot!!.child("thumb_image").value.toString()
                                        var displayName = dataSnapshot!!.child("display_name").value.toString()
                                        holder.messengerTextView!!.text = displayName
                                        Picasso.with(holder.profileImageViewRight!!.context)
                                                .load(imageUrl)
                                                .placeholder(R.drawable.profile_img)
                                                .into(holder.profileImageViewRight)
                                    }

                                    override fun onCancelled(databaseError: DatabaseError?) {
                                    }
                                })
                    } else {
                        //to the other person show image view to left side
                        holder.profileImageViewRight!!.visibility = View.GONE
                        holder.profileImageViewLeft!!.visibility = View.VISIBLE
                        holder.messageTextView!!.gravity = (Gravity.CENTER_VERTICAL or Gravity.LEFT)
                        holder.messengerTextView!!.gravity = (Gravity.CENTER_VERTICAL or Gravity.LEFT)

                        mFirebaseDatabase!!.child("Users")
                                .child(userId)
                                .addValueEventListener(object : ValueEventListener {
                                    override fun onDataChange(dataSnapshot: DataSnapshot?) {
                                        var imageUrl = dataSnapshot!!.child("thumb_image").value.toString()
                                        var displayName = dataSnapshot!!.child("display_name").value.toString()
                                        holder.messengerTextView!!.text = displayName
                                        Picasso.with(holder.profileImageViewLeft!!.context)
                                                .load(imageUrl)
                                                .placeholder(R.drawable.profile_img)
                                                .into(holder.profileImageViewLeft)
                                    }

                                    override fun onCancelled(databaseError: DatabaseError?) {
                                    }
                                })
                    }
                }
            }
        }
        //Set recycler view
        messageRecyclerView.layoutManager = layoutManager
        messageRecyclerView.adapter = mFirebaseAdapter

        messengerSendButton.setOnClickListener {
            if (!intent.extras.get("name").toString().equals("")) {
                var currentUserName = intent.extras.get("name")
                var currentUserId = mFirebaseUser!!.uid
                var friendlyMessage = FriendlyMessage(currentUserId, messageEdt.text.toString().trim(), currentUserName.toString())
                mFirebaseDatabase!!.child("messages").push().setValue(friendlyMessage)
                messageEdt.setText("")
            }
        }

    }

    public override fun onStart() {
        super.onStart()
        mFirebaseAdapter!!.startListening()
    }

    public override fun onStop() {
        super.onStop()
        mFirebaseAdapter!!.stopListening()
    }

    class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageTextView: TextView? = null
        var messengerTextView: TextView? = null
        var profileImageViewLeft: CircleImageView? = null
        var profileImageViewRight: CircleImageView? = null
        fun bindView(friendlyMessage: FriendlyMessage) {
            messageTextView = itemView.findViewById(R.id.messengerMessage)
            messengerTextView = itemView.findViewById(R.id.messengerSender)
            profileImageViewLeft = itemView.findViewById(R.id.messengerImageViewLeft)
            profileImageViewRight = itemView.findViewById(R.id.messengerImageViewRight)

            messageTextView!!.text = friendlyMessage.message
            messengerTextView!!.text = friendlyMessage.sender
        }
    }
}
