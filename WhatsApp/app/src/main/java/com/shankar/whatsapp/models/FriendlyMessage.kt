package com.shankar.whatsapp.models

class FriendlyMessage() {
    var id: String? = null
    var message: String? = null
    var sender: String? = null

    constructor(id: String?, message: String?, sender: String?): this(){
        this.id = id
        this.message = message
        this.sender = sender
    }
}