package com.shankar.whatsapp.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

import com.shankar.whatsapp.R
import com.shankar.whatsapp.adapters.UsersAdapter
import com.shankar.whatsapp.models.Users
import kotlinx.android.synthetic.main.fragment_users.*

class UsersFragment : Fragment() {

    var databaseReference: DatabaseReference? = null
    var usersAdapter: UsersAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        databaseReference = FirebaseDatabase.getInstance().reference.child("Users")
        usersRecyclerView.setHasFixedSize(true)
        usersRecyclerView.layoutManager = layoutManager
        var firebaseRecyclerOptions = FirebaseRecyclerOptions.Builder<Users>()
                .setQuery(databaseReference!!, Users::class.java)
                .build()
        usersAdapter =  UsersAdapter(firebaseRecyclerOptions, context!!)
        usersRecyclerView.adapter = usersAdapter
    }

    override fun onStart() {
        super.onStart()
        usersAdapter!!.startListening()
    }

    override fun onStop() {
        super.onStop()
        usersAdapter!!.stopListening()
    }
}
