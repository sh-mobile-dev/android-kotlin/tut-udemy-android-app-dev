package com.shankar.whatsapp.models

class Users() {
    var display_name: String? = null
    var image: String? = null
    var status: String? = null
    var thumb_image: String? = null

    constructor(displayName: String?, displayImage: String?, thumbImage: String?, userStatus: String?) : this(){
        this.display_name = displayName
        this.image = displayImage
        this.status = thumbImage
        this.thumb_image = userStatus
    }
}