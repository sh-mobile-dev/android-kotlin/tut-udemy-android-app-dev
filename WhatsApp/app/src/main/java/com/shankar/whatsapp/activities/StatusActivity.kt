package com.shankar.whatsapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.shankar.whatsapp.R
import kotlinx.android.synthetic.main.activity_status.*

class StatusActivity : AppCompatActivity() {

    var mDatabase: DatabaseReference? = null
    var mCurrentUser: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status)

        supportActionBar!!.title = "Status"

        if(intent.extras != null){
            var oldStatus = intent.extras.get("status")
            statusUpdateET.setText(oldStatus.toString())
        }
        statusUpdateButton.setOnClickListener {
            mCurrentUser = FirebaseAuth.getInstance().currentUser
            var userId = mCurrentUser!!.uid

            mDatabase = FirebaseDatabase.getInstance().reference
                    .child("Users")
                    .child(userId)
           var status = statusUpdateET.text.toString().trim()
            mDatabase!!.child("status")
                    .setValue(status).addOnCompleteListener {
                        task: Task<Void> ->
                        if (task.isSuccessful){
                            Toast.makeText(this, "Status updated successfully", Toast.LENGTH_SHORT).show()
//                            startActivity(Intent(this, SettingsActivity::class.java))
                            finish()
                        }
                        else{
                            Toast.makeText(this, "Status update failed", Toast.LENGTH_SHORT).show()
                        }
                    }
        }
    }
}
