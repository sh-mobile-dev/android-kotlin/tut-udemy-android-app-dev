package com.shankar.listviewactivity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var namesAdapter: ArrayAdapter<String>
        var namesArray: Array<String> = arrayOf("Prabha","Sangeetha","Sangeetha","Prabha","Sangeetha","Prabha","Prabha","Sangeetha","Prabha","Sangeetha","Prabha","Sangeetha","Prabha","Sangeetha","Prabha","Sangeetha","Prabha")
        namesAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, namesArray)
        mListView.adapter = namesAdapter

        mListView.setOnItemClickListener { parent, view, position, id ->
            //parent : parent view
            //view : view
            //position : position of each view
            //id : id of the row
            Toast.makeText(applicationContext, "ID : $id" , Toast.LENGTH_SHORT).show()
            var itemName : String = mListView.getItemAtPosition(position).toString()
            Toast.makeText(applicationContext, "Item Name : $itemName" , Toast.LENGTH_SHORT).show()
        }
    }
}
