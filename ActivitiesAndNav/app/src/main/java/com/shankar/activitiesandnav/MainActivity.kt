package com.shankar.activitiesandnav

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nextActivity.setOnClickListener {
            var intent = Intent(this, Main2Activity::class.java)
            intent.putExtra("name" , "Prabha")
            startActivityForResult(intent, REQUEST_CODE)
//            startActivity(Intent(this, Main2Activity::class.java))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK){
                    var toDisplay = data!!.extras.getString("return")
                    Toast.makeText(applicationContext, toDisplay, Toast.LENGTH_SHORT).show()
                }
            }
            else->{
                Toast.makeText(applicationContext, "No request made", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
