package com.shankar.activitiesandnav

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity(), View.OnClickListener {
    var name = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        var data = intent.extras
        if(data != null){
            name = data.getString("name")
            Toast.makeText(applicationContext,name,Toast.LENGTH_LONG).show()
            imageView.setImageDrawable(application.getDrawable(R.drawable.smiley))
        }
        backButton.setOnClickListener{
            var returnIntent = this.intent
            returnIntent.putExtra("return", "Hello " + name)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()    //gets rid of the activity from stack
        }
        imageView.setOnClickListener(this)
    }
    override fun onClick(p0: View?) {
        when (p0!!.id){
            imageView.id -> {
                Toast.makeText(applicationContext, "Image clicked", Toast.LENGTH_SHORT).show()
            }
        }
    }

}
