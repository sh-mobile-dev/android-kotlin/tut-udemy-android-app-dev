package com.shankar.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val PREFS_NAME = "myPrefs"
    var myPrefs: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Get data from shared prefs
        var dataBack: SharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        //To check whether the Preference File contains a particular value
        if (dataBack.contains("message")) {
            var myMessage = dataBack.getString("message", "")
            textView.text = myMessage
        }

        button.setOnClickListener {
            //getSharedPreferences(file_name, mode)
            myPrefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
            var editor: SharedPreferences.Editor = myPrefs!!.edit()

            //With TextUtils.isEmpty we can check whether certain object is empty or not
            if (!TextUtils.isEmpty(editText.text.toString())) {
                //Edit text is not empty
                var message = editText.text.toString()
                editor.putString("message", message)
                editor.commit()
            } else {
                Toast.makeText(this, "Please enter something", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
