package com.shankar.recyclerview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.shankar.recyclerview.data.PersonListAdapter
import com.shankar.recyclerview.model.Person
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var adapter: PersonListAdapter? = null
    private var personList: ArrayList<Person>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        personList = ArrayList<Person>()
        layoutManager = LinearLayoutManager(this)
        adapter = PersonListAdapter(personList!!, this)


        //setup list for recycler view
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter

        //load data
        for (i in 0..9){
            personList!!.add(Person("Sangeetha" + i , 20 + i))
        }
        adapter!!.notifyDataSetChanged()
    }
}
