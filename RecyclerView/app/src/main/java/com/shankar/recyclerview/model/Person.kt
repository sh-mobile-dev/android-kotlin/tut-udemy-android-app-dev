package com.shankar.recyclerview.model

class Person{
    var personName: String? = null
    var age: Int? = null
    constructor(name:String, age:Int){
        this.personName = name
        this.age = age
    }
}