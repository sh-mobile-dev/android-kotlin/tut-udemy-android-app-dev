package com.shankar.recyclerview.data

import com.shankar.recyclerview.model.Person
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.shankar.recyclerview.R

class PersonListAdapter(private val list: ArrayList<Person>,
                        private val context: Context): RecyclerView.Adapter<PersonListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //Create view from xml file
        val view = LayoutInflater.from(context).inflate(R.layout.list_row, parent, false)
        return ViewHolder(view)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }
    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindItem(item: Person){
            //binding a list item attributes to appropriate views
            var name: TextView = itemView.findViewById(R.id.nameTextView)
            var age: TextView = itemView.findViewById(R.id.ageTextView)
            name.text = item.personName
            age.text = item.age.toString()

            itemView.setOnClickListener{
                Toast.makeText(context, name.text, Toast.LENGTH_SHORT).show()
            }
        }
    }



}