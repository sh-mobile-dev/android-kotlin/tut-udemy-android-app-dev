package com.shankar.whatsmyname

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        updateButton.setOnClickListener {
            var enteredText =  enterNameEdt.text
            resultText.text = enteredText
        }
    }
}
